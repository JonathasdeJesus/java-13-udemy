package arrays;

import java.util.Arrays;

public class Exercicio {
	public static void main(String[] args) {
		double[] notasAlunoA = new double[4];
		
		notasAlunoA[0] = 7.9; // forma fixa, acessando cada um dos elementos. 
		notasAlunoA[1] = 8; 
		notasAlunoA[2] = 6.7;
		notasAlunoA[3] = 9.7;
		
		
		//Arrays.toString(notasAlunoA)
		System.out.println(Arrays.toString(notasAlunoA));
		System.out.println(notasAlunoA[0]); // Para ler um elemento 
		System.out.println(notasAlunoA[notasAlunoA.length - 1]); // Para ler a ultima nota do aluno. 
		
		double totalAlunoA = 0; 
		
		for(int i =0; i < notasAlunoA.length; i++) { // length ja sabe quanto que tem no array. 
		 totalAlunoA += notasAlunoA[i]; 
			
		}
		
		System.out.println(totalAlunoA / notasAlunoA.length); // Dividido pelo numero de Arrays que temos. 
		
		
		 final double notaArmazenada = 5.9;
		double[] notasAlunoB = { 6.9, 8.9, notaArmazenada, 10 }; // Outra forma de inicializar um array 
		
		
		double totalAlunoB = 0;
		for (int i = 0; i < notasAlunoB.length; i++) {
			totalAlunoB += notasAlunoB[i]; 
			
			
		}
		System.out.println(totalAlunoB / notasAlunoB.length);
		
		
		
	}

}

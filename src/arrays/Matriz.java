package arrays;
import java.util.Arrays;
import java.util.Scanner;

public class Matriz {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);

		System.out.println("Quantos alunos? "); // Primeira pergunta
		int qtdeAlunos = entrada.nextInt();  // Resposta da 1 pergunta
		
		
		System.out.println("Quantas notas por aluno? "); // Segunda pergunta
		int qtdeNotas = entrada.nextInt();  // resposta 2 pergunta 
		
		double [][] notasDaTurma = new double [qtdeAlunos][qtdeNotas];  // Criando Arrays
		
		
		double total = 0; // Variavel nota total 
      		for (int a = 0; a < notasDaTurma.length; a++) { // Criando o for para percorrer a matriz e colocar as notas. ( no caso do aluno ) 
			for (int n = 0; n < notasDaTurma[a].length; n++) { // Percorrer as notas do aluno 
				
				
				
				System.out.printf("Informe a nota %d do aluno %d ", n + 1, a + 1); // Interagindo com o usuario . 
				notasDaTurma[a][n] = entrada.nextDouble(); // Aluno recebe a nota que for digitada. 
				total += notasDaTurma[a][n]; 
 			}
		}
		
		double media = total / (qtdeAlunos * qtdeNotas ); // Caculo media final,  ( total,  divido pela quantidade de alunos x quantidade de notas) 
		// Ex. Se temos 3 aluno, 3 notas. a media sera: 9 notas ao todo, divido por 9 . 
		
		System.out.println("Media da turma e :"  + media); // Exibindo a media :)
		
		
		for (double[] notasDoAluno: notasDaTurma) { // FOR mais simples. 
		System.out.println(Arrays.toString(notasDoAluno)); // Valor das notas da turma. Para isso precisa fazer o for 
		}
		entrada.close();
	}

}

package fundamentos.operadores;

public class Ternarios {
	public static void main(String[] args) {
		// atribuicao condicional 
		
		double media = 7.6; 
		
	
		String resultadoFinal = media >= 7.0 ? "Aprovado" : "em recuperacao."; //condicional, Operador ternario  " ? " 
			           //   expressao  |       Valores    o dois ponto separa se expressao e verdadeira ou falsa. 
						System.out.println("O aluno esta:  " + resultadoFinal );
						
						
						
						
						double nota = 9.9; 
						boolean bomComportamento = false; 
						boolean passouPorMedia = nota >= 7; 
						boolean temDesconto = bomComportamento && passouPorMedia; 
						String resultado = temDesconto ? "Sim" : "Nao.";   // Usando os ternario, para converter um true e false em sim ou n�o. 
						System.out.println("Tem desconto?  " + resultado);
			
						
	}

}

package fundamentos.operadores;

public class DesafioLogicos {
	public static void main(String[] args) {
		//Trabalho na ter�a ( V ou F ) 
		//Trabalho na quinta ( V ou F ) 1 trabalho de cert  tv 32 , 2 deu certo 50 a tv . a familia vai tomar sorvete. 
		// Se nenhum dos dois der certo, v�o ficar em casa. 
		
		boolean trabalho1 = true; 
		boolean trabalho2 = false; 
		
		boolean comprouTv50 = trabalho1 && trabalho2; 
		boolean comprouTv32 = trabalho1 ^ trabalho2; 
		boolean comprouSorvete = trabalho1 || trabalho2; 
		
		boolean maisSaudavel = !comprouSorvete;  // Operador Un�rio!
		
		
		
		System.out.println("Comprou Tv 50\" ? " + comprouTv50);
		System.out.println("Comprou Tv 32\" ? " + comprouTv32);
		System.out.println("Comprou Sorvete ? " + comprouSorvete);
		
		// Operador Un�rio!
		System.out.println("Mais saud�vel ?" + maisSaudavel);
		
	}

}

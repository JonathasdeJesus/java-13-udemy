package fundamentos.operadores;

public class Logicos {
	public static void main(String[] args) {
		
		boolean condicao1 = true;
		boolean condicao2 = 3 > 7; // Maior 
		
		System.out.println(condicao1 && !condicao2); //Verdadeiro / falso 
		System.out.println(condicao1 || condicao2); // OU 
		System.out.println(condicao1 ^ condicao2); // Ou exclusivo.  (verdadeiro)
		System.out.println(!!condicao1); // Dupla nega��o, resultado verdadeiro. 
		System.out.println(!condicao2); // N�o condi��o ( dara verdadeiro, pq 3 n�o � maior que 7 {Deu falso no real} ) 
		
		// Tabela verdade E 
		
		System.out.println("\n\nTabela verdade E (AND)");
		System.out.println(true && true);
		System.out.println(true && false);
		//System.out.println(false && true);
		//System.out.println(false && false);
		
		System.out.println("\n\nTabela verdade OU (OR)");
		//System.out.println(true || true);
		//System.out.println(true || false);
		System.out.println(false || true);
		System.out.println(false || false);
		
		System.out.println("\n\nTabela verdade  OU Exclusivo (XOR)");
		System.out.println(true ^ true);
		System.out.println(true ^ false);
		System.out.println(false ^ true);
		System.out.println(false ^ false);
		
		
		System.out.println("\n\nTabela verdade NOT"); // Operador un�rio. Tabela de nega��o .
		System.out.println(!true);
		System.out.println(!false);
		
	} 

}

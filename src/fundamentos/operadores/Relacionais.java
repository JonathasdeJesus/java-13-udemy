package fundamentos.operadores;

public class Relacionais {
	public static void main(String[] args) {
		int a = 97; 
		int b = 'a';
		
	System.out.println(a == b); // Comparacao e o == 
	System.out.println(3 > 4); // 3 e maior que 4 
	System.out.println(3 >= 3); // 3 e maior ou igual a 3 
	System.out.println(30 <= 7 ); // 30 e menor ou igual a 7 
	System.out.println(30 != 7 ); // 30 e diferente ou igual a 7 

	double nota = 7.3;
	boolean bomComportamento = true; 
	boolean passouPorMedia = nota >= 7; 
	
	boolean temDesconto = bomComportamento && passouPorMedia; 
	
	System.out.println("Tem desconto ?  " + temDesconto);
	   // System.out.println('\u0061'); // Imprimindo um numero unicod  ( nao e legal usar essa parte aqui). 
	}

}

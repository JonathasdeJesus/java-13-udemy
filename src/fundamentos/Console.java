package fundamentos;

import java.util.Scanner;

public class Console {
	
	public static void main(String[] args) {
		System.out.print("Bom");
		System.out.print(" dia!\n");
		
		System.out.println("Bom");
		System.out.println("dia!");
		
		System.out.format("Megasena:  %d %d %d %d %d %d \n", 1,2,3,4,5,6);// Os caracteres "%d" ser�o substituido pelos n�meros que viram em seguida.
		
		System.out.printf("Sal�rio : %.1f \n ", 1234.56789);
		System.out.printf("Nome : %s \n", "Jonathas");
		
		
		//int a = 3 ; Entrada de dados --- Tipo --- Nome --- Valor ---
		
		Scanner entrada = new Scanner(System.in); //Usuario digitou o nome, em seguida mostro a mensagem a ele. 
		
		System.out.print("Digite seu nome: "); // Aqui pede para o usuario digiar o nome; saida de dados
		String nome = entrada.nextLine();	// Aramazena o nome 
		
		System.out.print("Digite seu sobrenome: "); // Aqui pede para o usuario digiar o nome; saida de dados
		String sobrenome = entrada.nextLine();	// Aramazena o nome
		
		System.out.print("Digite a sua idade: ");
		int idade = entrada.nextInt();
		
		System.out.println("\n\nNome :  " + nome + " " + sobrenome + "\nIdade:  " + idade + " anos."); // mostra o resultado de tudo .
		System.out.printf("%s %s tem %d anos.\n", nome, sobrenome, idade); // MOstra todo resultado por�m formatado j� . 
		
		entrada.close(); // tem que fechar o Scanner .
		
		
		
		
		
	}

}
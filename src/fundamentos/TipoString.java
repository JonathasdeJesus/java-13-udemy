package fundamentos;

public class TipoString {

	public static void main(String[] args) {
      System.out.println("Ol� Pessoal!".charAt(2)); // Esse charAt, identifica letra  no indice (Pegar letras especificas)   
      
      String s ="Bom dia"; 
      s = s.toUpperCase();
      System.out.println(s.concat("!!!"));
      System.out.println(s + "!!!!outro");
      System.out.println(s.startsWith("B")); // Verifica se Inicia com aquela palavra informada entre parenteses.
      System.out.println(s.toLowerCase().startsWith("bom")); //Verifica se come�a com a palavra informada, N�o verifica maiusculas e minusculas.  
      System.out.println(s.length()); // Conta quantos caracter tem a String.
      
      System.out.println(s.endsWith("dia")); // VErifica se o final da frase. 
      System.out.println(s.equalsIgnoreCase("BOM DIA")); // Ignora M/m 
      
      String nome = "Pedro";  // var n�o funciona. 
      String sobrenome = "Santos"; // Ficou como String porque o double so ler numero � n�ao caracter, string e s� caracter 
      double idade = 33;  // ficou como double, porque "var" s� � a partir do JRE 10. Estou com o 8 . 
      double salario = 12345.987; // ficou como double, porque "var" s� � a partir do JRE 10. Estou com o 8 . 
      
      System.out.println("Nome: " + nome + "\nSobrenome: " + sobrenome + "\nIdade: " + idade + "\nSal�rio: " + salario + "\n\n");
      
      System.out.printf("Nome %s %s %s e ganha R$%.2f.", nome, sobrenome, idade, salario); // Esse dolar s, substitui j� as variaveis  //%d substitui valor inteiros  //f flutuante
      
      String frase = String.format("\n\nNome %s %s %s e ganha R$%.2f.", nome, sobrenome, idade, salario); // Forma de criar string, 
      System.out.println(frase);
      
      // Explorando API da String 
      
      System.out.println("Frase qualquer ".contains("qual")); // verifica se cont�m a palavra naquela frase 
      System.out.println("Frase qualquer ".indexOf("qual")); // Qual indice da palavra ( onde come�a a palvra) 
      System.out.println("Frase qualquer\n ".substring(6)); // Pega um treco sa String, e comea a ler 
      System.out.println("Frase qualquer ".substring(6,8)); // Pega um treco da String, e come�a a ler... E para depois da ultima informada. 
      
      
      
      
	}

}

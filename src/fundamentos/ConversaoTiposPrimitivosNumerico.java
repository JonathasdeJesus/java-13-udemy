package fundamentos;

public class ConversaoTiposPrimitivosNumerico {

	public static void main(String[] args) {
		
		double a  = 1; //implicita
      System.out.println(a);
      
      float b =(float)  1.23456788888; // explícita (CAst)
      System.out.println(b);
      
      int C = 30; 
      byte d = (byte) C; // explícita (CAst)
      System.out.println(d);
      
      double e = 1.99999999; 
      int f = (int) e; // explícita (CAst)
      System.out.println(f);
	}

}

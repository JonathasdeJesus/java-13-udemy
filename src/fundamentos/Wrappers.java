package fundamentos;

import java.util.Scanner;

import javax.sound.sampled.BooleanControl;

public class Wrappers {
	
	public static void main(String[] args) {
		
		//Scanner entrada = new Scanner(System.in);	 Da para realizar com o Scanner tamb�m.
		
		
		
		
		// Byte
		Byte b = 100; 
		Short s = 1000; 
		Integer i = 100000;
				                                             //Integer.parseInt(entrada.next()); //  Outra forma de integer inteiro 
		Long l = 100000L;
		
		System.out.println(b.byteValue());
		System.out.println(s.toString());
		System.out.println(i * 3);                                 // Pega valor digitado na linhas 13 e j� multiplica por 3 e mostra na tela. 
		System.out.println(l / 3);
		
		Float f = 123.0F;
		System.out.println(f);
		
		Double d = 1234.5678;
		System.out.println(d);
		
		Boolean bo = Boolean.parseBoolean("true"); // Converter String para booleano 
		System.out.println(bo);
		System.out.println(bo.toString().toUpperCase()); // Converter Boolean / string
		
		
		Character c = '#'; // char 
		System.out.println(c + "...");
		
		//entrada.close();
	}

}

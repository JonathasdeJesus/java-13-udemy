package fundamentos;

public class NotacaoPonto {

	public static void main(String[] args) {

		String s = "Bom dia X";
		s = s.replace("X", "Senhora"); // troca uma palavra pela outra .
		s = s.toUpperCase(); // Aumenta todas letras
		s = s.concat("!!!"); // Mesma coisa que o " = + " .

		System.out.println(s);

		System.out.println("Leo".toUpperCase()); // Vai mostrar j� o Leo em maiusculo

		String x = "Leo".toUpperCase(); // Aqui tab�m mostra o leo em maiusculo,o por�m usa 2 maneiras
		System.out.println(x);

		String y = "Bom dia X" // Inserimos todos pontos, e exibimos na tela no final.
				.replace("X", "Gui")
				.toUpperCase()
				.concat("!!!");

		System.out.println(y);

		// Tipos primitivos n�o tem o operador "."

		int a = 3;
		System.out.println(a);
		
		

	}

}

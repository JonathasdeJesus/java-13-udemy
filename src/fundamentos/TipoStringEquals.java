package fundamentos;

import java.util.Scanner;

public class TipoStringEquals {
	public static void main(String[] args) {
		System.out.println("2" == "2");
		
		String s1 = new String ("2"); // Muito dificil usar, forc a barra assim. 
		System.out.println("2" == s1);
		System.out.println("2".equals(s1)); // Porque o equals compara os conteudos e nao entra em questao interna da linguagem.
		
		Scanner entrada = new Scanner (System.in); 
		
		String s2 = entrada.nextLine(); //Esse next deixa os espacos em brancos.
		
		System.out.println("2" == s2.trim()); // trim ignora os espacos em brancos 
		System.out.println("2".equals(s2.trim()));		
		
		entrada.close(); 
	}

}

package fundamentos;

public class AreaCircunferencia {

	public static void main(String[] args) {

		double raio = 3.4;
		final double PI = 3.14159;

		double area = PI * raio * raio;

		System.out.println(area);
		System.out.println("Valor da �rea � : " + area);
		
		raio = 10;
	    area = PI * raio * raio;
	    System.out.println("Area igual " + area + " m2.");
		
	
	}

}

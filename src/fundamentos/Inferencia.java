package fundamentos;

public class Inferencia {

	public static void main(String[] args) {
		double a = 4.;
		System.out.println(a);

		double b = 4.5;
		System.out.println(b);
		
		a = 12;
		System.out.println(a);
		
		double d; 
		d = 123.65;
		System.out.println(d);
		
		
	}
}

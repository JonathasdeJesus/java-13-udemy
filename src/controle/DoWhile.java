package controle;

import java.util.Scanner;

public class DoWhile {
	
	public static void main(String[] args) {
		
	
		Scanner entrada = new Scanner(System.in);

		String texto = ""; 	
		
		do {
			System.out.println("Voce precisa falar as palavras magica...");
			System.out.print("Quer sair?");
			texto = entrada.nextLine();
			
		}   while(!texto.equalsIgnoreCase("por favor")); // Enquanto texto for diferente de por favor, vai ficar perguntando. 
		
		
		System.out.println("Obrigado");

		entrada.close();
	}

}

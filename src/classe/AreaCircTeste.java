package classe;

public class AreaCircTeste {
	public static void main(String[] args) { 
		
		AreaCirc a1 = new AreaCirc (10); // Calculo de Area criando instancia
		a1.raio = 10; 
		
	
		
		
		System.out.println(a1.area()); // Area calculada pea instancia 
		
		System.out.println(AreaCirc.area(100)); // Area calculada pertenccente a classe
		
		
		
		System.out.println(AreaCirc.PI); // vers�o do PI que definimos 
		System.out.println(Math.PI); // vers�o do PI que o Java defini 
 	}
	
	
}
// Mudou o PI nao apenas para instancia a2 , mais sim para todas instancia da classe  "area circulo"  
//a1.pi = 1000000;  Essa forma nao e a ideal para acessar 	

//variavel Pi refatou para ser constante 

		//a2.pi = 0; Essa forma deixa de ser a melhor para acessar. Mais sim pelo nome da classe
		
		//AreaCirc.PI = 3.1415;
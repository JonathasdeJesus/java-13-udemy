package classe;

public class AreaCirc {
	
	double raio;
	final static double PI = 3.14 ; // Variavel pertence a classe, nao mais instancia 
	
	 AreaCirc(double raioInicial) { // Agora valor de pi esta associado a clase e nao instancia 
		// pi = 3.14; // Aqui estamos deixando o pi para altera��o	
		 raio = raioInicial;

	}  
	 double area () {
		 return PI * raio * raio;
		 //return pi *Math.pow(raio, 2); // Outra maneira de retornar a area 
		 // Funcao pow dentro de Math, nao precisou criar  Math m = new Math();  
		 // pow e um metodo estatico, nao precisa criar uma instancia da classe Math para acessar ele. 
 	 }

	 
	 static double area(double raio) { 
		 return PI *Math.pow(raio, 2); // Metodo estatico 
		 
	 }
}

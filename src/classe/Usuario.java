package classe;

public class Usuario { 
	
	String nome; 
	String email;
	
	
	public boolean equals(Object objeto) { // Faz a conversao do object para o objeto 
		
		if(objeto instanceof Usuario) { // Se objeto e uma instancia usuario, podemos implementar a logica. 
		
		Usuario outro = (Usuario) objeto; // Comparar ususario com usuario 
		
		
		boolean nomeIgual = outro.nome.equals(this.nome); // variavel verificar os nomes.  
		boolean emailIgual = outro.email.equals(this.email);
		
		
		
		return nomeIgual && emailIgual ; // Vai retornar a comparacao 
		
		} else { 
			return false;
			
		   } 
		
	    }
		
	}  



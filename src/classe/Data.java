package classe;

public class Data {

	int dia;
	int mes;
	int ano;

	Data() { // Construtor padrao para a data, que vai receber uma data padrao 1/1/1970
		//dia = 1;
		//mes = 1;
		//ano = 1970;
         this(1,1,1970);         
	}

	Data(int dia, int mes, int ano) { // Construtor que ja recebe seus devido valores
		this.dia = dia; // Para nao conflitar as variaveis, usamos o "Inicial "
		this.mes = mes;
		this.ano = ano;

	}

	String obterDataFormatada() {

		return String.format("%d/%d/%d ", dia, mes, ano);
	}

	void imprimirDataFormatada() {
		System.out.println(obterDataFormatada());
	}

}

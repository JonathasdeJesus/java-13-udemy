package classe;

public class DataTeste { 
	public static void main(String[] args) {
		
		Data d1 = new Data(); // Retiro esse valor do d1, porem eles estaram no contrutor padrao  "Data"  
		d1.dia = 2021; //Retirado, pois o construtor padrao ja chama a data 
		//d1.mes = 11; Retirado pois o construtor padrao ja chama a data	
		//d1.ano = 2021; Retirado pois o construtor padrao ja chama a data
		
		
		System.out.printf(d1.obterDataFormatada());
		
	   Data d2 = new Data(31,12,2020); // Criado a segunda instancia 
	   //d2.dia = 31; Ja chamamos em cima 
	  // d2.mes = 12; 
	   //d2.ano = 2020; 
	   
	   String dataFormatada1 = d1.obterDataFormatada();
	   
	   System.out.println(dataFormatada1);
	   System.out.println(d2.obterDataFormatada());
		 
	   
	   
	   d1.obterDataFormatada();
	   d2.obterDataFormatada();
		
	}

}
 
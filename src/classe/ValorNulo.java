package classe;

public class ValorNulo { 
	
	public static void main(String[] args) {
		
		String s1 = "";
		System.out.println(s1.concat("!!!!!"));
		
		Data d1 = Math.random() > 0.5 ? new Data() : null; // Se for maior que 0.5, vai criar nova data, caso contrario, valor null.
		
		if(d1 != null) { // Se variavel d1 for diferente de nulo .
		     d1.mes = 3;
		     System.out.println(d1.obterDataFormatada());
		}
		
			String s2 = Math.random() > 0.5 ? "Opa" : null; // cria visualizacao aleatoria
			
		if(s2 != null) {
		System.out.println(s2.concat("???"));
		}
	}
}
package classe;

import java.util.Date;

public class Equals { 
	
	public static void main(String[] args) {
		
		Usuario u1 = new Usuario();
		u1.nome = "Pedro Silva";
		u1.email = "pedro.silva@ezemail.com.br";
		
		Usuario u2 = new Usuario();
		u2.nome = "Pedro Silva";
		u2.email = "pedro.silva@ezemail.com.br";
		
		System.out.println(u1==u2); // Comparacao do usuario 1 ao usuario 2 
		System.out.println(u1.equals(u2)); // Equals por padrao, retorna mesmo valor que u1==u  ( false)
		System.out.println(u2.equals(u1)); // Retorna mesmo valor que o primeiro.
		
		//System.out.println(u1 == u1);  Mesmo endereco de memoria, por isso verdadeiro 
		//System.out.println(u1.equals(u1));  Verdadeiro, por conta do espaco de memoria 
		
		
		System.out.println(u2.equals(new Date())); // Dara um erro, caso na classe Usuario n�o fizer o if da linha 11 
	}

}

package Lambdas;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;

public class OperadorBinario {
	
	public static void main(String[] args) {
		
		BinaryOperator<Double> media = (n1, n2) -> (n1 + n2) / 2; 
		
		System.out.println(media.apply(9.8,5.7));
		
		BiFunction<Double, Double, String> resultado = (n1, n2) ->  {
			double notaFinal = (n1 + n2) / 2; // calcula  a media 
			return notaFinal >= 7 ? "Aprovado" : "Reprovado"; // mostra se e aprovado ou nao. 
		};
		
		
	 // Funcao lambda que tem mais responsabilidade
		          										//Se for maior que 7 significa que resultado e aprovado caso contrario reprovado.  	
		
		System.out.println(resultado.apply(9.7, 5.1));
		
		Function<Double, String> conceito = m -> m >= 7 ? "Aprovado" : "Reprovado" ; // 
		
		System.out.println(media.andThen(conceito).apply(9.7, 4.1));
		
	}

}

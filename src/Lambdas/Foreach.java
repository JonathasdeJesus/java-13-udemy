package Lambdas;

import java.util.Arrays;
import java.util.List;

public class Foreach {
	
	public static void main(String[] args) {
		
		List<String> aprovados = Arrays.asList("Ana", "Bia", "Lia", "Gui"); 
		
		System.out.println("Forma tradicional...");
		
		for(String nome: aprovados) {
			System.out.println(nome);
		} 
		
		System.out.println("\nLambdas 01 ");
		aprovados.forEach(nome -> System.out.println(nome + " Aqui eu tenho varias formas de personalizar !!! ")); // Criar expressao lambda  e passar para o foreach
		// Aqui consegue personalizar. 
		
		
		
		
		
		
		System.out.println("\nMethod Reference...");
		aprovados.forEach(System.out::println); // Passando uma referencia para a funcao. 
		// Para cada aprovado chame o println, imprima na saida do  sistema. 
		
		
		System.out.println("\nLambdas 02 ");
		aprovados.forEach(nome -> meuImprimir(nome)); // expressao lambda passada  para o metodo foreach
		
		
		System.out.println("\nMethod Reference 02...");
		aprovados.forEach(Foreach::meuImprimir);

		
		
	}
	
	static void meuImprimir(String nome) {
		System.out.println("Oi, meu nome é : " + nome);
	} 
	

}

package Lambdas;

import java.util.function.Function;

public class Funcao {
	
	public static void main(String[] args) {
		
		Function<Integer, String>  parOuImpar =   numero -> numero % 2 == 0 ? "Par" : "Impar";
	// Interface funcional         	variavel          expressao lambda  ( numero modulo 2 for igual a zero, vai retornar par, caso contrario impar. 
		
		Function<String, String> oResultadoE = valor -> "O resultado e: " + valor; 
		
		
		Function<String, String> empolgado = valor -> valor + "!!!!"; 
		
		Function<String, String> duvida = valor -> valor + "?????";
		
		
		
		String resultadoFinal1 = parOuImpar.andThen(oResultadoE).andThen(empolgado).apply(32);
		//                chama parOuImpar e entao 
		
		System.out.println(resultadoFinal1);
		
		String resultadoFinal2 = parOuImpar.andThen(oResultadoE).andThen(duvida).apply(33); // No caso de estar  com duvidas
		System.out.println(resultadoFinal2);
		
		
		
		
		System.out.println(parOuImpar.apply(32)); // coloca um numero e depois retorna que e impar ou par. 
	}

}

package Lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Consumidor {
	
	public static void main(String[] args) {
		
		Consumer<Produto> imprimirNome = p -> System.out.println(p.nome + " !!!"); // receber o produto e imprimir apenas o nome. 
		
		
		Produto p1 = new Produto("Caneta", 12.34, 0.09);
		imprimirNome.accept(p1); // Para imprimir o nome.
		
		
		Produto p2 = new Produto("Nootbook", 2987.99, 0.25);
		Produto p3 = new Produto("Caderno", 19.90, 0.03);
		Produto p4 = new Produto("Borracha", 7.80, 0.18);
		Produto p5 = new Produto("Lapis", 4.39, 0.19);
		Produto p6 = new Produto("Teste Jonathas", 100, 0.10);
		
		
		
		List<Produto> produtos = Arrays.asList(p1, p2, p3, p4, p5, p5);
		
		produtos.forEach(imprimirNome); // recebe um parametro e nao retorna nada
		produtos.forEach(p -> System.out.println(p.preco)); // chamando de forma manual 
		produtos.forEach(System.out::println); // imprimindo o produto , se nao tiver o to string imprime de uma forma diferente. 
	}

}

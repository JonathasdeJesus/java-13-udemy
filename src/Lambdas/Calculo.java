package Lambdas;


@FunctionalInterface // obriga a usar apenas um metodo (funcao) 
public interface Calculo {
	
	 double executar(double a, double b); 

	 default String legal() {
		  return "legal"; 
	 }
	 
	 static String muitoLegal () {
		 return "muito legal"; 
	 }
}

package Lambdas;

import java.util.function.Predicate;

public class PredicadoComposicao {
	
	public static void main(String[] args) {
		
		Predicate<Integer> isPar = num -> num % 2 == 0; 
		Predicate<Integer> isTresDigitos = num -> num >= 100  && num <= 999; 
		// numero maior ou igual a 100      e  numero for menor ou igual a 999
	
		
		
		
		System.out.println(isPar.and(isTresDigitos).negate().test(123)); // verifica se e par 
		System.out.println(isPar.or(isTresDigitos).test(123));
	//                   e par ou tem 3digitos 
	
	}                      


}

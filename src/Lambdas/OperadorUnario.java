package Lambdas;

import java.util.function.UnaryOperator;

public class OperadorUnario {
	public static void main(String[] args) {
		
		UnaryOperator<Integer>  maisDois = n -> n + 2;
		UnaryOperator<Integer>  vezesDois = n -> n * 2;
		UnaryOperator<Integer>  aoQuadrado = n -> n * n;
		
		
		int resultado1 = maisDois //andThen vai na sequencia 
				.andThen(vezesDois)
				.andThen(aoQuadrado)
				.apply(0);
		System.out.println(resultado1);
		
		
		int resultado2 = aoQuadrado
				.compose(vezesDois)
				.compose(maisDois).
				apply(0); // comeca o proceso, nessa caso comeca pelo maisDois > vezesDois > aoQuadrado
		
		System.out.println(resultado2);
		
	}
}

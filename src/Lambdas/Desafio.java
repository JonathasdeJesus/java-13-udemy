package Lambdas;

import java.util.function.Function;
import java.util.function.UnaryOperator;

public class Desafio { 
	public static void main(String[] args) {
		
		 // Produto p = new Produto("Ipad", 3235.89, 0.13); deixar esse codigo la embaixo para nao dar erro com o nome da lambda 
		
		// 1. A partir do produto calcular o preco real (com desconto)
		//2. Imposto Municipal : >= 2500 (8,5%) / < 2500 (Isento)
		//3. Frete : >=3000 (100) / < 3000 (50)
		//4. Arredondar: Deixar duas casas decimais
		//5. Formatar: 1234,56.
		
		Function<Produto, Double> precoFinal = produto -> produto.preco * (1 - produto.desconto); 
		UnaryOperator<Double> impostoMunicipal= precoImposto -> precoImposto >= 2500 ? precoImposto * 1.085 : precoImposto; // Se preco for maior ou igual a 2500 significa que estou acrescentando 8,5%... caso contrario  retorna o mesmo preco
		UnaryOperator<Double> frete = preco -> preco >= 3000 ? preco  + 100 : preco + 50;
		
		
		UnaryOperator<Double> redondo =  preco1 -> Double.parseDouble(String.format("%.2f", preco1));   //retorna um valor com ponto flutuante o %f
		
		
		Function<Double, String> formatar = precoFormatar -> ("R$" + precoFormatar).replace(".", ","); // replace substitui o ponto pela virgula 
		
		
		Produto p = new Produto("Ipad", 3235.89, 0.0);
		
		String preco = precoFinal
				.andThen(impostoMunicipal)
				.andThen(frete)
				//.andThen(redondo)
				.andThen(formatar)
				.apply(p);
		
		System.out.println("Preco final : " + p);
		
	}

}

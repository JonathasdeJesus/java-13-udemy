package Lambdas;

public class CalculoTeste2 {
	
	public static void main(String[] args) {
		
		Calculo calc = (x,y) -> { return x + y; }; // Funcao lambda // funcao anonima que foi definido 
		 // calc e do tipo calculo (interface) 
		
		System.out.println(calc.executar(2, 3));
		
		calc = (x, y) ->  x * y; // lambda express ( Quando nao se coloca chaves, automaticamente aquilo que colocarmos sera retornado.
		System.out.println(calc.executar(2, 3));
		
		System.out.println(calc.legal());
		System.out.println(Calculo.muitoLegal());
		
	}

}

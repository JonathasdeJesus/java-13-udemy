package colecoes;

import java.util.LinkedList;
import java.util.Queue;

public class Fila {
	public static void main(String[] args) {
		
		Queue<String> fila = new LinkedList<>(); 
		
		//Offer e Add -> adicionam elementos na fila 
		// Diferenša e o comportamento ocorre
		//quando a fila esta cheia !
		
		fila.add("Ana"); // Retorna false
		fila.offer("Bia 1"); // lanca uma excecao  // caso nao consiga adicionar na fila . 
		fila.add("Carlos 2");
		fila.offer("Daniel 3 ");
		fila.add("Rafaela 4 ");
		fila.offer("Gui 5 "); // Tenta adicionar na fila, porem se tiver um limite de pessoas na fila, ele retorna falso.  
		
	
		// Peek e Element -> obter o proximo elementos
		//da fila ( sem remover) 
		// Diferenša e o comportamento ocorre
		//quando a fila esta vazia !
		System.out.println(fila.peek()); // Retorna false ( null)
		System.out.println(fila.peek()); // Lanca uma excecao 
		System.out.println(fila.element());
		System.out.println(fila.element());
		
		
		// POll e remove-> obter o proximo elementos 
		// da fila e remove
		
		System.out.println(fila.poll()); // retorna false
		System.out.println(fila.remove()); // lanca uma excecao
		System.out.println(fila.poll());
		System.out.println(fila.poll());
		System.out.println(fila.poll());
		System.out.println(fila.poll());
		
		
		
		// fila.size()
        // fila.clear(); limpar fila		
		// fila.isEmpty() saber se a fila esta vazia 
		
		//fila.contains(o) para saber se objeto esta ou nao contido. 
		
		
	}

}

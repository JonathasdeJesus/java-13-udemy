package colecoes;

import java.util.ArrayDeque;
import java.util.Deque;

public class Pilha {
	
	public static void main(String[] args) {
		
		Deque<String> livros = new ArrayDeque<>(); 
		
		livros.add("O pequeno principe"); // retorna uma valor boolean , retorna true ou false ( se adicionar ou nao ) 
		livros.push("Don Quixote"); // Nao adiciona nada caso esteja cheio a pilha, gerando um erro. 
		livros.push("O Hobbit");
		
	System.out.println(livros.peek());
	System.out.println(livros.element());
	
	for(String livro: livros) {
		System.out.println(livro);
	}
	
	
	System.out.println(livros.poll()); // Remover da pilha 
	System.out.println(livros.poll());
	System.out.println(livros.poll());
	System.out.println(livros.poll());
//	System.out.println(livros.remove()); tem excecao 
//	System.out.println(livros.pop());
	
	
	livros.size(); // Tamanho da pilha 
	livros.clear(); // para limpar 
	//livros.contains();
	//livros.isEmpty() // se est� vazio 
	

	
      
	}

}

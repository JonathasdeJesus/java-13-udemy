package colecoes;

public class Usuario { 
	String nome;

	Usuario(String nome) { // Para criar o usuario. 
		this.nome = nome;
		
	}
	
	
	public String toString() { // Quando imprimir um objeto do tipo usuario 
		return "Meu nome � " + this.nome + "."; 
		
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
	// Para Gerar esse hashCode e Equals >> Aperta na tela com o bot�o direito do mouse em / Source/  Generate hashCode () and equals()... 
}

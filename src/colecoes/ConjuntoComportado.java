package colecoes;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class ConjuntoComportado {
	
	public static void main(String[] args) {
		
		 //Set<String> listaAprovados = new HashSet<>(); // Esse hashSet nao segue a ordem alfabetica de exibicao dos nomes 
		
		
		 SortedSet<String> listaAprovados = new TreeSet<>();  // Aceita ordem de ordenacao o sorteset
		 
		 listaAprovados.add("Ana");
		 listaAprovados.add("Carlos");
		 listaAprovados.add("Luca");
		 listaAprovados.add("Pedro");
		 
		 for(String candidato: listaAprovados) { // Percorrer a lista 
			 System.out.println(candidato);
		 }
		 
		 Set<Integer> nums = new HashSet<>(); // so inteiros
		 
		 nums.add(1);
		 nums.add(2);
		 nums.add(120);
		 nums.add(6);
		 
		 for(int n: nums) { // Percorrer a lista nums
			 System.out.println(n);
		 }
		 
	
	}

}

package colecoes;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Mapa { 
	
	public static void main(String[] args) {
		
		Map<Integer, String> usuarios = new HashMap<>();
		usuarios.put(1,"Roberto"); 
		usuarios.put(2,"Ricardo ");
		usuarios.put(3,"Rafaela ");
		usuarios.put(4,"Rebeca");
		usuarios.put(500,"Jonathas");

		
		System.out.println(usuarios.size());
		System.out.println(usuarios.isEmpty()); // Verifica se tem elemento ou nao ( retorna false)
		
		System.out.println(usuarios.keySet()); // pega as chaves [1, 2, 3, 4]
		System.out.println(usuarios.values()); // pega os valores [Roberto, Ricardo, Rafaela, Rebeca]
		System.out.println(usuarios.entrySet()); // pega as chaves e os valores ao mesmo tempo. [1 = roberto .....]
		
		System.out.println(usuarios.containsKey(4)); // Verifica se contem a chave (true)
		System.out.println(usuarios.containsValue("Rebeca")); // verifica se contem o valor (true) 
		
		System.out.println(usuarios.get(4)); // pega a chave e retorna o valor ( Rebeca)
		System.out.println(usuarios.remove(1)); // Remove a partir da chave  ( Roberto) 
		System.out.println(usuarios.remove(4, "Gui")); // Remove a partir da chave e o valor ( false) 
		
		// 3 maneira para percorrer o MAP 
		
		for(int chave: usuarios.keySet()) { // Percorrendo a chave 
			System.out.println(chave);
		}  
		
		for(String valor: usuarios.values()) { // Percorrer pelo valor . 
			System.out.println(valor);
		}
		
		for(Entry<Integer, String> registro: usuarios.entrySet()) { // Pega valores e chave
			System.out.print(registro.getKey() + " == >>>  ");
			System.out.println(registro.getValue());
		}
 	}

}

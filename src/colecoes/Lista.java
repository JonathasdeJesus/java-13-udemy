package colecoes;

import java.util.ArrayList;

//import classe.Usuario;

public class Lista { 
	
	public static void main(String[] args) {
		ArrayList<Usuario> lista = new ArrayList<>(); 
		
		Usuario u1 = new Usuario("Ana"); 
		lista.add(u1);
		
		lista.add(new Usuario ("Carlos"));  // Criou o objeto e colocou direto na coluna. 
		lista.add(new Usuario ("Lia"));
		lista.add(new Usuario ("Bia"));
		lista.add(new Usuario ("Manu"));
		
		System.out.println(lista.get(3)); // Imprimir um elemento atrav�s do ID  ( acessar pelo indice)
		
		// lista.remove(1); // Remover por indice
		System.out.println( ">>>>>>>>>" + lista.remove(1)); // visuzalizar remove , removido por indice
		
		//lista.remove(new Usuario ("Manu"));  // Remover a partir de um obejto.
		System.out.println(lista.remove(new Usuario ("Manu"))); // VIsualizar o remove ( true) 
		
		System.out.println("Tem o elemento selecionado a seguir ? :  " + lista.contains(new Usuario("Lia"))); // Verfica se esta contido ou nao (contem o elemento) 
		
		for(Usuario u: lista) // Percorrer o usuario 
			System.out.println(u.nome); // Imprimir 
		
		
		
		
		
	}

}
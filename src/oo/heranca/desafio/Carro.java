package oo.heranca.desafio;

public class Carro { 

	    public final int VELOCIDADE_MAXIMA;
		protected int velocidadeAtual; 
		private int delta = 5;
		
		
		protected Carro (int velocidadeMaxima){
			VELOCIDADE_MAXIMA = velocidadeMaxima;
		}
		
		public void acelerar() { // metodo 
			
			if(velocidadeAtual + getDelta() > VELOCIDADE_MAXIMA ) {
				velocidadeAtual = VELOCIDADE_MAXIMA;
			} else {
				velocidadeAtual += getDelta();	
			}
			
			
			
		}
		
		public void freiar() { //metodo para restricao 
		if(velocidadeAtual >= 5) { // s� vai freiar se a velocidade for mais ou igual a 5 
			velocidadeAtual -= 5;
			} else { // caso contrario, velocidade atual =0
			      velocidadeAtual = 0;
			
			        }
			}
		
		 public String toString() { // metodo para retornar String  
			return "Velocidade atual " + velocidadeAtual + "Km/h."; 
			
		
		}   // Implementando get e set do delta ( delta private)

		public int getDelta() {
			return delta;
		}

		public void setDelta(int delta) {
			this.delta = delta;
		}
		 
		 
	
		
	

}

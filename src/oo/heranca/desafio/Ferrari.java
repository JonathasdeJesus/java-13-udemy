package oo.heranca.desafio;

public class Ferrari extends Carro implements Esportivo, Luxo{ 
	
	private boolean ligarTurbo = false;
	private boolean ligarAr = false;
	
	public Ferrari(){ // Criado construtor padrao
		this(315);
	}
	
	public Ferrari(int velocidadeMaxima) { // construtor 
		super(velocidadeMaxima);
		setDelta(15);
		//delta = 15;
	//}
	//@Override // So serve de efeito para validacao ( para sobre escrever um metodo que ja existe) 
	//void acelerar() {
	//	velocidadeAtual += 15; // fazendo uma atribuicao da ferrari
	
		
		
	}
	@Override
	public void ligarTurbo() {
		ligarTurbo = true; 
		//setDelta(35);
		
		
	}
	
	@Override
	public void desligarTurbo() {
		ligarTurbo = false;
		setDelta(15);
		
	
		
	}
	
	@Override
	public void ligarAr() {
		ligarAr = true; 
			
		
	}
	@Override
	public void desligarAr() {
		ligarAr = false;
		
		//sobre escrever  o metodo get 
	
	
		
	}
	
	@Override
	public int getDelta() {
		if(ligarTurbo && !ligarAr) {  // Se ligar turbo e ar desligado
			return 35;
			
		} else if(ligarTurbo && ligarAr)  {
			return 30;
		} else if(!ligarTurbo && !ligarAr) { // Se nao ligar turbo e nao ligar ar 
			return 20;
		} else {
		
		    return 15;
		}
		
	}

}

package oo.heranca.teste;

import oo.heranca.Direcao;
import oo.heranca.Heroi;
import oo.heranca.Monstro;

public class Jogo {
	
	public static void main(String[] args) {
		
		Monstro monstro = new Monstro(); // Criando jogador
		monstro.x = 10; // posicao do jogador 
		monstro.y = 11;
		
		
		Heroi heroi = new Heroi(10,12); // atribuindo o valor de x e y 
		//heroi.x = 10;
		// heroi.y = 12;
		
		
		
		System.out.println("Vida do monstro: " + monstro.vida); 
		System.out.println("Vida do Heroi: " + heroi.vida);
		
		monstro.atacar(heroi);
		heroi.atacar(monstro);
		
		monstro.atacar(heroi);
		heroi.atacar(monstro);
		
		monstro.andar(Direcao.NORTE); 		
		monstro.atacar(heroi);
		heroi.atacar(monstro);
		
		System.out.println("Vida do monstro, depois do ataque: " + monstro.vida);
		System.out.println("Vida do heroi, depois do ataque: " + heroi.vida);
	}

}

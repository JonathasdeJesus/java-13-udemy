package oo.heranca.teste;

import oo.heranca.desafio.Carro;
import oo.heranca.desafio.Civic;
import oo.heranca.desafio.Ferrari;

public class CarroTeste {
	
	public static void main(String[] args) {
		
		Carro c1 = new Civic(); 
		
		c1.acelerar();
		System.out.println(c1);
		
		c1.acelerar();
		System.out.println(c1);
		
		c1.acelerar();
		System.out.println(c1);
		
		Ferrari c2 = new Ferrari(400); // atribuindo a velocidade maxima para ferrari. 
		c2.ligarTurbo();
		
		
		System.out.println(c2.velocidadeDoAr());
		
		
		c2.acelerar();
		c2.freiar();
		System.out.println(c2);
		
		c2.acelerar();
		c2.freiar();
		System.out.println(c2);
		
		c2.acelerar();
		System.out.println(c2);
		
		
	}

}

package oo.encapsulamentoCasaB;

import oo.encapsulamentoCasaA.Ana;

public class Pedro extends Ana { // Pedro herda da Ana.
	
	void testeAcessos() {
		
		
		// Ana mae = new Ana(); Quando e por heranca, nao precisa estanciar uma Ana, e acesso as forma diretamente.   
		
		//System.out.println(facoDentroDeCasa); // N�o � transferido por heran�a quando n�o est� no mesmo pacote. ( visibilidade pacote) 
		System.out.println(formaDeFalar); // So acessa por heranca e nao por instancia 
		//System.out.println(segredo);
		System.out.println(todosSabem);
		System.out.println(new Ana().todosSabem);
	}
	
	
	

}

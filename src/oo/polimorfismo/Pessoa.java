package oo.polimorfismo;

public class Pessoa {
	
	private double peso;
	
	public Pessoa (double peso) { // Construtor publico 
		setPeso(peso);
	}

	public void comer(Comida comida) { // metodo comer arroz.
		this.peso +=comida.getPeso();
	}

	
	
	
	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		if (peso >= 0) {
			this.peso = peso;	// Nao permite setar o peso com valor negativo 
		} // Nao permite setar o peso com valor negativo 
	
	} 

}

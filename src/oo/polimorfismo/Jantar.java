package oo.polimorfismo;

public class Jantar { 
	
	public static void main(String[] args) {
		
		Pessoa convidado = new Pessoa(99.65); // Peso da pessoa
		
		Arroz ingrediente1 = new Arroz(0.20); // Quantidade de arroz que o convidado ira comer 
		Feijao ingrediente2 = new Feijao(0.10); // Quantidade de feijao que o convidado ira comer. 
		 
		System.out.println(convidado.getPeso()); // VER 
		
		convidado.comer(ingrediente1); // Atribuicao de novo peso 0.20
		convidado.comer(ingrediente2); // Atribuicao do segundo peso 0.10
		
		System.out.println(convidado.getPeso()); // VER
		
		Sorvete sobremesa = new Sorvete(0.4);
		
		convidado.comer(sobremesa);		
		System.out.println(convidado.getPeso());
		
		
	}

}

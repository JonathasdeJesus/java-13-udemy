package oo.composicao;

import java.util.ArrayList;
import java.util.List;

public class Aluno {
	
	final String nome; 
	final List<Curso> cursos = new ArrayList<>();	
	
	Aluno(String nome) {   // construtor aluno 
	      this.nome = nome;
	}
	void adicionarCurso( Curso curso) { // Adicionar o curso
		this.cursos.add(curso);
		curso.alunos.add(this);
		
	}
	
	 Curso obterCursoPorNome(String nome) { // Criando outro  metodo dentro de aluno que procura o curso com aquele nome. 
		 for(Curso curso: this.cursos) { // Para cada curso, 
			 if(curso.nome.equalsIgnoreCase(nome)) { //Se curso for igual  ao parametro, retorna curso.
				 return curso;
			 }
		 }
		 
		 return null; 
	 }
	
	public String toString() { // metodo, o que retornar sera impresso na classe "CursoTeste" na linha 31   ( converter objeto para string)
		return nome;
	}
	

}

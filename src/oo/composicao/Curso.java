package oo.composicao;

import java.util.ArrayList;
import java.util.List;

public class Curso {
	
	final String nome; 
	final List<Aluno> alunos = new ArrayList<>();	
	
	Curso(String nome) { //construtor do curso 
		this.nome = nome;
	}
	
	void adicionarAluno(Aluno aluno){ // Criando metodo para adicionar aluno
		this.alunos.add(aluno); // 
		aluno.cursos.add(this);
		
	}
	

}

package oo.composicao;

public class CompraTeste {
	
	public static void main(String[] args) {
		
		Compra compra1 = new Compra(); 
		compra1.cliente = "Joao Pedro"; 
		compra1.adicionarItem("Caneta", 20, 7.45); // criando novo item 
		compra1.adicionarItem(new Item("Borracha", 12, 3.89)); 
		compra1.adicionarItem(new Item("Caderno", 3, 18.79)); // assim que adiciono "adicionar item", estou relacionando a compra. Fazendo a relacao bidirecional 
		
		System.out.println(compra1.itens.size()); // Tamanho dos itens.
		System.out.println(compra1.obterValorTotal());
		
		
		
		// So para mostrar a relacao bidirecional
		double total = compra1.itens.get(0).compra.itens.get(1).compra.obterValorTotal();
		System.out.println("O valor � " + total);
	

	}
}
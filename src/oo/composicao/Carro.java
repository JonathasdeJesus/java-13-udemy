package oo.composicao;

public class Carro {
	
	Motor motor = new Motor();
	
	void acelerar() {
		if(motor.fatorInjecao < 2.6) { // Se  for menor que 2.6 
		motor.fatorInjecao += 0.4;
	}
	}
	
	void frear() {
		if(motor.fatorInjecao > 0.5) { // Se motor for maior do que 0.5 faz a mudan�a 
		motor.fatorInjecao -= 0.4;
		}
	}
	
	void ligar () {
		motor.ligado = true; 
	}
	
	void desligar() {
		motor.ligado = false;
	}

	boolean estaLigado() {
		return motor.ligado;
	}
	
}

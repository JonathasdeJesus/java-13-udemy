package oo.composicao;

public class Item {
	
	String nome; 
	int quantidade;
	double preco; 
	Compra compra; // Bidirecional.
	
	Item(String nome, int quantidade, double preco) { // construtor que recebe os 3 atributos
		this.nome = nome; 
		this.quantidade = quantidade; 
		this.preco = preco; 
	}
	

}

package oo.composicao.desafio;

import java.util.ArrayList;
import java.util.List;

public class Compra {
	
	final List<Item> itens = new  ArrayList<>(); // relacao compra/item
	
	void adicionarItem(Produto p, int qtde) { // metodo adicionar item. 
		this.itens.add(new Item (p, qtde)); 
		
	} 
	
	void adicionarItem(String nome, double preco, int qtde) {
		Produto produto = new Produto(nome, preco);
		this.itens.add(new Item (produto, qtde)); 
	}
	
	
	
	double obterValorTotal() {
		double total = 0;
		
		for(Item item: itens ) {// percorrendo o item 
			total += item.quantidade * item.produto.preco;
		}
			return total;
		
	}
 	

}

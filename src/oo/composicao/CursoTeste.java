package oo.composicao;

public class CursoTeste {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno("Joao");
		Aluno aluno2 = new Aluno("Maria");
		Aluno aluno3 = new Aluno("Pedro");

		Curso curso1 = new Curso("Java Completo");
		Curso curso2 = new Curso("Web 2023");
		Curso curso3 = new Curso("React Native");

		curso1.adicionarAluno(aluno1); // Relacionamento entre os objetos. 
		curso1.adicionarAluno(aluno2);

		curso2.adicionarAluno(aluno1);
		curso2.adicionarAluno(aluno3);

		aluno1.adicionarCurso(curso3);
		aluno2.adicionarCurso(curso3);
		aluno3.adicionarCurso(curso3);

		for (Aluno aluno : curso3.alunos) { // pegar todos os alunos do curso listado 
			System.out.println("Estou matriculado no curso " + curso3.nome + "...");
			System.out.println("... e o meu nome � " + aluno.nome);
			System.out.println();
		}
		
		System.out.println(aluno1.cursos.get(0).alunos); // implementar o toString (linha 30 classe aluno)
		
		Curso cursoEncontrado = aluno1.obterCursoPorNome("Java Completo"); 
		
		if(cursoEncontrado != null) { // Se curso encontrado for diferente de nulo. 
			System.out.println(cursoEncontrado.nome); // listrar os alunos do curso 
			System.out.println(cursoEncontrado.alunos); // pego todos os alunos se o curso for encontrado. 
		}

	}

}

package oo.composicao;

import java.util.ArrayList;

public class Compra {
	
	String cliente;
	ArrayList<Item> itens = new ArrayList<Item>(); // Criando a lista, apenas item. 
	
	
	
	void adicionarItem(String nome, int quantidade, double preco) { // metodo de atributo 
		this.adicionarItem(new Item (nome, quantidade, preco));
	}
	
	void adicionarItem(Item item) { // adicionar item
		this.itens.add(item); // bidirecional 
		item.compra = this; // 
	}
	
	double obterValorTotal() { // Criado (metodo) construtor de calculo do valor total 
		double total = 0;
		
	
		
		for (Item item: itens) { // 
			total += item.quantidade * item.preco; // Cada um dos itens sera acrescentado ao valor total do item 
		}
		
		return total;
		
	}

}

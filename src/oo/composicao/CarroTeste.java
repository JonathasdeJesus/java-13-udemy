package oo.composicao;

public class CarroTeste {
	
	public static void main(String[] args) {
		
		Carro c1 = new Carro();
		System.out.println(c1.estaLigado()); // Esta ligado /? False, pq nao esta 
		
		c1.ligar(); // ligar o carro
		System.out.println(c1.estaLigado()); // ligou  true
		
		System.out.println(c1.motor.giros());
		
		c1.acelerar();
		c1.acelerar();
		c1.acelerar();
		c1.acelerar();

		System.out.println(c1.motor.giros());
		
		c1.frear();
		c1.frear();
		c1.frear();
		c1.frear();
		c1.frear();
		c1.frear();
		c1.frear();
		c1.frear();
		
		// Faltou Encapsulamento !!!
		//c1.motor.fatorInjecao = -30;
		
		System.out.print(c1.motor.giros());
	}

}

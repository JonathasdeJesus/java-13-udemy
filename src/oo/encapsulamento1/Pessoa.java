package oo.encapsulamento1;

public class Pessoa {
	
	private String nome;
	private String sobrenome;
	private int idade; // so pessoa pode alterar 
	
	
	public Pessoa(String nome, String sobrenome,  int idade) {
		setNome(nome);
		setSobrenome(sobrenome);
		setIdade(idade);
		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}



	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	// Metodo Getters  
	public int getIdade() {
		
		return idade;
		
	}
	
	public String getNomeCompleto() {
		return getNome() +  " " + getSobrenome();
	}; 
	
	//metodo Setter
	public void setIdade (int novaIdade) {
		novaIdade = Math.abs(novaIdade); // Passar valor absoluto (negativo vira positivo)
		if(novaIdade >= 0 && novaIdade <= 120 ) { // Validacao, se o valor da idade for igual ou maior a 0, (vai aplicar nova idade)
		this.idade = novaIdade;
		}
	}
	
	@Override
	public String toString() {
		return "Ola eu sou o " + getNome() + " e tenho " + getIdade() + " anos.";
	}

}

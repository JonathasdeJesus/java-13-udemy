package oo.encapsulamentoCasaA;

public class Ana {
	
	@SuppressWarnings("unused") // Suprimi a advertencia que nao esta sendo usada. 
	private String segredo = "...";
	String facoDentroDeCasa = "..."; // default ou pacote 
	protected String formaDeFalar = "...";
	public String todosSabem = "..."; 

}

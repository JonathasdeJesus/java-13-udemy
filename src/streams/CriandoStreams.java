package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class CriandoStreams {
	
	public static void main(String[] args) {
		
		Consumer<String> print = System.out::print;  // recebe um parametro e nao retorna nada ..
		Consumer<Integer> println = System.out::println; // Para imprimir a linha 30
		
		Stream<String> langs = Stream.of("Java ", "Lua ", "JS ", "PHP "," HTML ", " ##Primeiro Stream criado\n"); // para criar a stream, stream.of .metodo estatico 1 
		langs.forEach(print ); // Para imprimir ..
		
		
		String[] maisLangs = { "Python ", "Lisp ", "Perl ", "Go ", " ##Segundo array criado\n"}; // Criando Arrays de Strings
		
		Stream.of(maisLangs).forEach(print); // Para  exibir passando array 2 
		Arrays.stream(maisLangs).forEach(print); // passa o array inteiro .. ( criando outro stream) 3 
		Arrays.stream(maisLangs, 1, 3).forEach(print); // ai imprime com excessao do ultimo elemento ..
		
		List<String> outrasLangs = Arrays.asList("C ", "PHP ", "Kotlin\n"); // Criar Stream a partir das collection
		outrasLangs.stream().forEach(print);// Criando nova Branch teste 
		outrasLangs.parallelStream().forEach(print); // exibe na tela fora de ordem ( aleatorio) 
		
		//Stream.generate(() -> "a").forEach(print);; gera sem ordenacao, porem infinita
		//Stream.iterate(0, n -> n + 1).forEach(println); // gera um atras do outro
		 
 	}

}

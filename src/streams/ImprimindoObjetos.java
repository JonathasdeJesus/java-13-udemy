package streams;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class ImprimindoObjetos {
	
	public static void main(String[] args) {
		
		List<String> aprovados = Arrays.asList("Lu, Gui, Luca, Ana, Jonathas");
		
		
		System.out.println("Usando o For normal");
		for (int i = 0; i < aprovados.size(); i++) {
			System.out.println(aprovados.get(i)); //for normal 
		}
		
		
		System.out.println("Usando o Foreach");
		for(String nome: aprovados) {
			System.out.println(nome);  // foreach
 		}
		
		System.out.println("\nUsando Iterator");
		Iterator<String> iterator = aprovados.iterator(); // cria o iterator e cria o while para controlar o laco 
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		
		
		System.out.println("\nUsando Stream");
		Stream<String> stream = aprovados.stream();
		stream.forEach(System.out::println); // laco interno 
	}

}

package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Match {
	
	public static void main(String[] args) {
		
		Aluno a1 = new Aluno("Ana ", 7.1);
		Aluno a2 = new Aluno("Luna ", 7.1);
		Aluno a3 = new Aluno("Gui ", 8.1);
		Aluno a4 = new Aluno("Gabi ", 10);
		
		List<Aluno> alunos = Arrays.asList(a1, a2, a3, a4);
		
		Predicate<Aluno> aprovado = a -> a.nota >= 7; // filtrar todos os aprovados. 
		Predicate<Aluno> reprovado = aprovado.negate();
		
		System.out.println(alunos.stream().allMatch(aprovado)); // Verdadeiro ou falso, se todos foram aprovados 
		System.out.println(alunos.stream().anyMatch(aprovado)); // Verdadeiro ou falso, se todos algum esta aprovado
		System.out.println(alunos.stream().noneMatch(reprovado)); // Se nehnum deu match de reprovado dara true
	}

}
